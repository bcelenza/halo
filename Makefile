.PHONY: clean
clean:
	cargo clean

.PHONY: build
build:
	cargo build

.PHONY: test
test:
	cargo test -- --nocapture

.PHONY: release
release: clean build test

.PHONY: run
run:
	cargo run
