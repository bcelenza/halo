use axum::{
    routing::get,
    http::StatusCode,
    response::IntoResponse,
    Json, Router,
};
use serde::{Serialize};
use std::net::SocketAddr;
use tower_http::trace::TraceLayer;

#[tokio::main]
async fn main() {
    // Setup logging
    if std::env::var_os("RUST_LOG").is_none() {
        std::env::set_var("RUST_LOG", "halo=debug,tower_http=debug")
    }
    let format = tracing_subscriber::fmt::format()
        .with_thread_ids(true)
        .with_thread_names(true)
        .json();
    tracing_subscriber::fmt().event_format(format).init();

    // setup router
    let app = Router::new()
        // `GET /` goes to `root`
        .route("/health", get(health))
        .route("/status", get(status))
        .layer(TraceLayer::new_for_http());

    // listen
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    tracing::info!("Listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn health() -> &'static str {
    "OK"
}

async fn status() -> impl IntoResponse {
    let response = StatusResponse {
        status: String::from("OK"),
    };

    (StatusCode::OK, Json(response))
}

#[derive(Serialize)]
struct StatusResponse {
    status: String,
}
